var express = require('express');
var cluster = require('cluster');
var router = express.Router();
var User = require('../models/user');

/* GET home page. */
router.get('/', function (req, res, next) {
  var i = 0;
  var time = Date.now();
  // setTimeout(()=> {
  //   if (!(cluster.worker.id % 2)) {
  //     console.log(1);
  //     throw new Error('something went wrong!');
  //   } else {
  //     cluster.worker.kill();
  //   }
  // }, 5000);
  while (i < 10000000000) {
    i++;
  }
  var time2 = Date.now();
  res.json({title: 'Express', worker: cluster.worker.id, time: time2.valueOf() - time.valueOf()});
});

router.get('/create', (req, res, next)=> {
  User.create({firstName: 'alex1', lastName: 'alex1'},
    (err, user)=> {
      if (err) {
        console.error(err);
        return next(err);
      }
      res.json(user);
    });
});

router.get('/list', (req, res, next)=> {
  User.find()
    .exec((err, users)=> {
      if (err) {
        console.error(err);
        return next(err);
      }
      res.json(users);
    });
});

module.exports = router;
