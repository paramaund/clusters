## Siam Web App

Deploy instruction:
1. Install Docker-Engine following official guide at https://www.docker.com/products/docker depends on your OS.
2. Login to the GitLab account with:
   `sudo docker login registry.gitlab.com`

For development:
3. Clone project repository with:

   `git clone https://gitlab.com/freshcode-projects/siamweb.git`
   
3. Create docker container with project folder connected to it:

   `sudo docker run --name <container name(e.g. siam)> -v /path/to/siamweb:/home/daemon/siam_web_app --entrypoint='/bin/bash' -itd -p <your local port(e.g. 3000)>:3000 registry.gitlab.com/freshcode-projects/siamweb -s`

   P.S. your project will be accessible at `http://localhost:<your local port(e.g. 3000)>`
   
4. Create docker container with project folder connected to it for storybook:

   `sudo docker run --name <container name(e.g. siamstorybook)> -v /path/to/siamweb:/home/daemon/siam_web_app --entrypoint='/bin/bash' -itd -p <your local port(e.g. 9001)>:9001 registry.gitlab.com/freshcode-projects/siamweb -s`

   P.S. storybook will be accessible at `http://localhost:<your local port(e.g. 9001)>`

P.S. After you connected project's folder, from your host, inside docker's container, you continue working with your local folder on the host, but run project inside container.

     You can see list of existing containers and there status with:

     `sudo docker ps -a`

     After you've created containers, you can just use them without recreation in the future. Means you shouldn't repeat steps above each time on the same host.

5. Now you've got two docker's containers running as daemons. You could start or stop it using the following commands:

   `sudo docker start/stop <container name>`

   Example: `sudo docker stop siam` - this will stop container named siam.
   
6. In dev mode, by default, projects, inside containers, are not started. To manage this issue you'll need to connect to the container to be able to manage project status - start, build and etc.

   `sudo docker exec -it <container name> bash`
   
   P.S. to do that, container must be started first. By default, working directory is project directory. Now you can use package.json scripts.
   
