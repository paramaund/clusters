var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
  firstName: {type: String},
  lastName: {type: String}
});

module.exports = mongoose.model('User', userSchema);